#include <stdio.h>

#define MAXLEN 15

void reverse(char str[]);
char* itob(int n, char str[], int base);

int 
main(void)
{
  int n = 21474836;
  char str[MAXLEN];

  printf("%x\n", n);
  printf("%s\n", itob(n, str, 16));

  return 0;
}

void
reverse(char str[])
{
  int i, j;
  char temp;

  for (j = 0; str[j] != '\0'; j++)
    ;

  for (i = 0, --j; i < j; i++, j--) {
    temp = str[i];
    str[i] = str[j];
    str[j] = temp;
  }
}

char*
itob(int n, char str[], int base)
{
  int sign, i;

  /* to include the most -ve int */
  n = ((sign = n) < 0) ? -(n+1) : n-1;

  i = 0;
  do {
    if (n%base >= 0 && n%base <= 9)
      str[i++] = n%base + '0';
    else
      str[i++] = n%base + 'a' - 10;
  } while ((n /= base) > 0);

  if (sign <0)
    str[i++] = '-';

  /* part of "to include the most -ve int" */
  str[0] += 1;
  str[i] = '\0';

  reverse(str);

  return str;
}
