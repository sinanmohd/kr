#include <stdio.h>

#define TAB 8

int 
main(void)
{
  int input, count, space;

  count = 0;

  while ((input = getchar()) != EOF) {
    if (input == '\t') {
      space = (TAB - (count % TAB));

      for (space = (TAB - (count % TAB)); space > 0; space--) {
        printf(" ");
        count++;
      }
    }

    else if (input == '\n')
      count = 0;

    else {
      count++;
      printf("%c", input);
    }
  }

  return 0;
}
