#include <stdio.h>

#define MAX_LEN 10

// Programm to produce histogram based on the length of words

int
main()
{
  int i, j, input, word_len, big_words;
  int len[MAX_LEN];

  word_len = big_words = 0;
  for (i = 0; i< MAX_LEN; i++)
    len[i]=0;

  while ((input = getchar()) != EOF) {
    if(input == ' ' || input == '\n' || input == '\t') {
      if (word_len >= MAX_LEN)
        big_words++;
      else
        len[word_len]++;
      word_len = 0;
    }
    else 
      word_len++;
  }

  if(input != ' ' && input != '\n' && input != '\t') {
    if (word_len >= MAX_LEN)
        big_words++;
      else
        len[word_len]++;
  }

  printf("\n");

  for (i = 0; i< MAX_LEN; i++) {
    printf(" %d -- %3d: ", i, len[i]);
    for (j =0; j < len[i]; j++)
      printf("*");
    printf("\n");
  }

  printf(">%d -- %3d: ", (MAX_LEN-1), big_words);
  for (j =0; j < big_words; j++)
      printf("*");
  printf("\n");
}

