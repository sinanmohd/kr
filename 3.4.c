#include <stdio.h>

#define MAXLEN 15

void reverse(char str[]);
char* itoa(int n, char str[]);

int 
main(void)
{
  int n = 2147483647;
  char str[MAXLEN];

  printf("%d\n", n);
  printf("%s\n", itoa(n, str));

  return 0;
}

void
reverse(char str[])
{
  int i, j;
  char temp;

  for (j = 0; str[j] != '\0'; j++)
    ;

  for (i = 0, --j; i < j; i++, j--) {
    temp = str[i];
    str[i] = str[j];
    str[j] = temp;
  }
}

char*
itoa(int n, char str[])
{
  int sign, i;

  /* to include the most -ve int */
  n = ((sign = n) < 0) ? -(n+1) : n-1;

  i = 0;
  do {
    str[i++] = n%10 + '0';
  } while ((n /= 10) > 0);

  if (sign <0)
    str[i++] = '-';

  /* part of "to include the most -ve int" */
  str[0] += 1;
  str[i] = '\0';

  reverse(str);

  return str;
}
