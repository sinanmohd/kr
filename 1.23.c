#include <stdio.h>

#define OUT 0
#define STAR 1
#define SLASH 2

/* this is a c programm to remove all comments from c programms */
// including "slash comments" 
/* and "star comments" */
// without the use of arrays

int 
main(void)
{
  char input, temp;
  int state;

  state = OUT;

  while ((input = getchar()) != EOF) {

    if (input == '/') {
       temp = input;

      if ((input = getchar()) == '*')
        state = STAR;
      else if (input == '/')
        state = SLASH;
      else 
        printf("%c", temp);

    }

    else if (input == '*' && state == STAR) {

      if ((input = getchar()) == '/') {
        state = OUT;

        if ((input = getchar()) != '\n')
          printf("%c", input);

        continue;
      }  
    }

    else if (input == '\n' && state == SLASH) {
      state = OUT;
      continue;
    }

    if (state == OUT)
      printf("%c", input);
  }

  return 0;
}
