#include <stdio.h>

char lower(char intput);

int 
main(void)
{
  char intput;

  while((intput = getchar()) != EOF)
    printf("%c", lower(intput));

  return 0;
}

char 
lower(char intput)
{
  return (intput >= 'A' && intput <= 'Z') ? intput-'A'+'a' : intput;
}
