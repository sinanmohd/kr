#include <stdio.h>
#include <string.h>

int strend(char *to, char *from);

int 
main(void)
{
  char str[] = "gnu linux";

  if(strend(str, "ux"))
    printf("yeahoooo\n");

  return 0;
}

int 
strend(char *to, char *from)
{
  size_t fsize = strlen(from);
  size_t tsize = strlen(to);

  if (tsize > fsize)
    to += tsize - fsize;
  else 
    return 0;


  while (*to++ == *from++ && *to != '\0' && *from != '\0')
    ;
  
  return (*from == '\0') ? 1 : 0;
}
