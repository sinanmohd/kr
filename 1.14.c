#include <stdio.h>

#define MAX_CHAR 128 // ASCII set

int t0;

/* program to print a histogram of the frequencies of diffrent 
 * characters in its input */

int t1;

// this is a test1

int t2;

/* trrrrrr2 */

int 
main(void)
{
  int i, j, input;
  int char_count[MAX_CHAR];

  for (i = 0; i < MAX_CHAR; i++)
    char_count[i] = 0;

  while((input = getchar()) != EOF) {
    char_count[input]++;
  }

  printf("\n");

  for (i =0; i < MAX_CHAR; i++) {
    if (char_count[i] == 0)
      continue;

    if (i == '\t')
      printf(" \\t -- %3d: ", char_count[i]);
    else if (i == '\n')
      printf(" \\n -- %3d: ", char_count[i]);
    else if (i == ' ')
      printf("' ' -- %3d: ", char_count[i]);
    else
      printf("  %c -- %3d: ", i, char_count[i]);

    for (j = 0; j <= char_count[i]; j++)
      printf("*");
    printf("\n");
  }
}
