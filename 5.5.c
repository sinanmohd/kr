#include <stdio.h>

char *strncpy(char to[], const char from[], int till);
char *strncat(char to[], const char from[], int till);
int strncmp_sneed(const char str1[], const char str2[], int till);

int 
main(void)
{
  char str[10];
  printf("%s\n", strncpy(str, "gnu linux", 3));

  printf("%s\n", strncat(str, " linux", 6));

  if (!strncmp_sneed("seed", "seed feed", 3))
    printf("suckses\n");

  return 0;
}

char *strncpy(char to[], const char from[], int till)
{
  if (from[till] != '\0')
    to[till+1] = '\0';

  while (till--)
    *(to+till) = *(from+till);

  return to;
}

char *strncat(char to[], const char from[], int till)
{
  char *to_og = to;

  while (*to)
    ++to;

  while (till--)
    *(to+till) = *(from+till);

  return to_og;
}

int 
strncmp_sneed(const char str1[], const char str2[], int till)
{
  int count;

  for (count = 0, --till; count < till && *(str1+count) == *(str2+count); count++)
    ;

  return *(str1+count) - *(str2+count);
}
