#include <stdio.h>

int 
main(void)
{
  char input;
  int num;

  num = 0;
  
  while ((input = getchar()) != EOF) {

    if (input == '0') {
      if ((input = getchar()) == 'x' || input == 'X')
        continue;
      else 
        input = '0';
    }

    if (!((input >= '0' && input <= '9') || (input >= 'a' && input <= 'f') || (input >= 'A' && input <= 'F')))
      continue;
    else if (input >= '0' && input <= '9')
      num = num * 16 + input - '0';
    else if (input >= 'a' && input <= 'f')
      num = num * 16 + 10 + input - 'a';
    else if (input >= 'A' && input <= 'F')
      num = num * 16 + 10 + input - 'A';

  }

  printf("\n%d\n", num);

  return 0;
}
