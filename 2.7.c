#include <stdio.h>

#define INT_NBITS 32

unsigned getbits(unsigned x, int from, int to);
void pbits(int x);
unsigned invert(unsigned x, int from, int to);

int 
main(void)
{
  int x = 3423834583;

  pbits(x);

  pbits(invert(x, 32, 32));
  
  return 0;
}

unsigned 
getbits(unsigned x, int from, int to)
{
  return (x >> (from+1-to)) & ~(~(unsigned)0 << to);
}

void 
pbits(int x)
{
  for (int i = INT_NBITS-1; i >= 0; i--) {
    if (i%4 == 3)
      printf(" ");

    printf("%u", getbits(x, i, 1));
  }

  printf("\n");
}

unsigned 
invert(unsigned x, int from, int to)
{
  return x ^ ((((unsigned)~0 << (INT_NBITS-from)) >> (INT_NBITS-to)) << (from-to));
}
