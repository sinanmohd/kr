#include <stdio.h>

#define TRUE 1

float conv_temp(int temp_c);

int 
main(void)
{
  int input, temp_c;
  while(TRUE) {
    printf("Enter temp in C: ");
    scanf("%d", &temp_c);
    printf("%d C is %.1f F\n", temp_c, conv_temp(temp_c));
  }

  return 0;
}

float 
conv_temp(int temp_c)
{
  return (9.0/5.0 * temp_c) + 32.0;
}
