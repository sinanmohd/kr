#include <stdio.h>

#define MAXLEN 30

char* itoa(char str[], int num);

int 
main(void)
{
  char str[MAXLEN];
  int num;

  num = -69420;

  printf("%d\n", num);
  printf("%s\n", itoa(str, num));

  return 0;
}

char*
itoa(char str[], int num)
{
  static int i = 0;

  if (num < 0) {
    num = -num;
    str[i++] = '-';
  }
  
  if (num/10)
    itoa(str, num/10);

  str[i++] = '0' + (num % 10);
  str[i] = '\0';

  return str;
}
