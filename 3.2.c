#include <stdio.h>

#define MAXLEN 5000 

char* escape(char to[], const char from[]);
char* etr(char to[], const char from[]);

int 
main(void)
{
  int i;
  char input;
  char str[MAXLEN];
  char e[MAXLEN];
  char r[MAXLEN];

  for (i = 0; (input = getchar()) != EOF && i+1 < MAXLEN; ++i)
    str[i] = input;

  str[i] = '\0';
  

  if (str[i-1] != '\n')
    printf("\n");

  printf("%s\n", escape(e, str));
  printf("%s\n", etr(r, e));

  return 0;
}

char* 
escape(char to[], const char from[])
{
  int i, j;

  for (i = j = 0; from[i] != '\0' && j+1 < MAXLEN; ++i)
    switch (from[i]) {
      case '\n' :
        to[j++] = '\\';
        to[j++] = 'n';
        break;
      case '\t' :
        to[j++] = '\\';
        to[j++] = 't';
        break;
      case '\a' :
        to[j++] = '\\';
        to[j++] = 'a';
        break;
      case '\b' :
        to[j++] = '\\';
        to[j++] = 'b';
        break;
      case '\f' :
        to[j] = '\\';
        to[j++] = 'f';
        break;
      case '\r' :
        to[j++] = '\\';
        to[j++] = 'r';
        break;
      case '\v' :
        to[j++] = '\\';
        to[j++] = 'v';
        break;
      default:
        to[j++] = from[i];
        break;
    }

  to[i] = '\0';

  return to;
}

char*
etr(char to[], const char from[])
{
  int i, j;

  for (i = j = 0; from[i] != '\0'; ++i) {
    if (from[i] == '\\')
      switch (from[++i]) {
        case 'n' :
          to[j++] = '\n';
          break;
        case 't' :
          to[j++] = '\t';
          break;
        case 'a' :
          to[j++] = '\a';
          break;
        case 'b' :
          to[j++] = '\b';
          break;
        case 'f' :
          to[j++] = '\f';
          break;
        case 'r' :
          to[j++] = '\r';
          break;
        case 'v' :
          to[j++] = '\r';
          break;
        default :
          to[j++] = '\\';
          to[j++] = from[i];
          break;
      }

    else
      to[j++] = from[i];
  }

  to[j] = '\0';

  return to;
}
