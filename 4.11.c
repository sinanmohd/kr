#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define MAXOP 100
#define NUMBER_SIG '0'

double pop(void);
void push(double f);
char getop(char str[]);

int 
main(void)
{
  char str[MAXOP];
  char type;
  double op2;

  while ((type = getop(str)) != EOF) {
    switch (type) {
      case NUMBER_SIG :
        push(atof(str));
        break;
      case '+' :
        push(pop() + pop());
        break;
      case '*' :
        push(pop() * pop());
        break;
      case '-' :
        op2 = pop();
        push(pop() - op2);
        break;
      case '/' :
        if ((op2 = pop()) == 0.0)
          printf("Err: deviser cant be zero\n");
        else 
          push(pop() / op2);

        break;
      case '%' :
        op2 = pop();
        if (op2 != 0)
          push(fmod(pop(), op2));
        else 
          printf("Err: deviser cant be zero\n");

        break;
      case '\n' :
        printf("%.8g\n", pop());
        break;
      default :
        printf("Err: unknown command\n");
    }
  }

  return 0;
}

#define MAXVAL 100
double val[MAXVAL];
int sp = 0;

double 
pop(void)
{
  if (sp > 0)
    return val[--sp];
  else {
    printf("Err: stack empty\n");
    return 0.0;
  }
}

void
push(double f)
{
  if (sp < MAXVAL)
    val[sp++] = f;
  else
    printf("Err: stack is full\n");
}


char 
getop(char str[])
{
  static char buff = ' ';
  char input;
  int i;

  /* ignore blanks */
  for (input = buff, str[0] = buff; isblank(input);)
    input = str[0] = getchar();
  
  buff = ' ';

  /* return operator */
  if (!isdigit(input) && input != '.')
    return input;

  /* collect digits*/
  for (i = 1; i < MAXOP && (isdigit(input=getchar()) || input == '.'); i++)
    str[i] = input;

  str[i] = '\0';

  if (input != EOF)
    buff = input;

  return NUMBER_SIG;
}
