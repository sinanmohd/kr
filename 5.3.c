#include <stdio.h>

char *strcat(char to[], char from[]);

int 
main(void)
{
  char str[10] = "glow";

  printf("%s\n", strcat(str, " bois"));

  return 0;
}

char 
*strcat(char to[], char from[])
{
  while(*to++)
    ;

  while ((*to++ = *from++))
    ;

  return to;
}
