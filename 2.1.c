#include <stdio.h>
#include <limits.h>
#include <float.h>

int 
main(void)
{
  unsigned char c;
  unsigned short s;
  unsigned int i;
  unsigned long l;

  c = ~0;
  s = ~0;
  i = ~0;
  l = ~0;

  c >>= 1;
  s >>= 1;
  i >>= 1;
  l >>= 1;

  /* signed values */

  printf("limits char   min: %20d, max: %20d\n", CHAR_MIN, CHAR_MAX);
  printf("comput char   min: %20d, max: %20d\n", -c-1, c);

  printf("limits short  min: %20d, max: %20d\n", SHRT_MIN, SHRT_MAX);
  printf("comput short  min: %20d, max: %20d\n", -s-1, s);

  printf("limits int    min: %20d, max: %20d\n", INT_MIN, INT_MAX);
  printf("comput int    min: %20d, max: %20d\n", -i-1, i);

  printf("limits long   min: %20ld, max: %20ld\n", LONG_MIN, LONG_MAX);
  printf("comput long   min: %20ld, max: %20ld\n", -l-1, l);

  printf("limits float  min: %20g, max: %20g\n", FLT_MIN, FLT_MAX);
  printf("limits double min: %20g, max: %20g\n", DBL_MIN, DBL_MAX);

  /* unsigned values */

  printf("limits uchar  min: %20d, max: %20u\n", 0, UCHAR_MAX);
  printf("comput uchar  min: %20d, max: %20u\n", 0, c*2+1);

  printf("limits ushort min: %20d, max: %20u\n", 0, USHRT_MAX);
  printf("comput ushort min: %20d, max: %20u\n", 0, s*2+1);

  printf("limits uint   min: %20d, max: %20u\n", 0, UINT_MAX);
  printf("comput uint   min: %20d, max: %20u\n", 0, i*2+1);

  printf("limits ulong  min: %20d, max: %20lu\n", 0, ULONG_MAX);
  printf("comput ulong  min: %20d, max: %20lu\n", 0, l*2+1);

  return 0;
}
