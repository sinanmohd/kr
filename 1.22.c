#include <stdio.h>

#define LINE_LENGTH 230
#define TAB 8

int 
main(void)
{
  int count;
  char input;

  count = 0;
  
  while ((input = getchar()) != EOF) {
    if (input == '\n')
      count = 0;
    else if (input == '\t')
      count = count + TAB;
    else 
      count++;

    printf("%c", input);

    if (count >= LINE_LENGTH) {
      printf("\n");
      count = 0;
    }
  }

  return 0;
}
