#include <stdio.h>

/* function to delete  each character in str1 that matches with str2 */
char* squeeze(char str[], const char cs[]);

int 
main(void)
{
  char str[] = "ok chud you win this time";
  char del[] = "oi";

  printf("%s\n", squeeze(str, del));

  return 0;
}

char*
squeeze(char str[], const char cs[])
{
  int i, j, k;

  for (i = j = 0; str[i] != '\0'; i++) {

    for (k = 0; cs[k] != '\0'; k++)
      if (str[i] == cs[k])
        break;

    if (cs[k] == '\0')
      str[j++] = str[i];
  }

  str[j] = '\0';

  return str;
}
