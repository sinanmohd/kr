#include <stdio.h>

int bitcount(unsigned num);

int 
main(void)
{
  int x = 1337; /* a random int */
  printf("%d\n", bitcount(x));;

  return 0;
}

int 
bitcount(unsigned num)
{
  int bits;

  for (bits = 0; num != 0; num &= (num-1))
    bits++;

  return bits;
}
