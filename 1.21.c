#include <stdio.h>

#define TAB 8

int 
main(void)
{
  int count, tab, blank;
  char input;

  count = 0;

  while((input = getchar()) != EOF) {
    
    if (input == ' ')
      count++;

    else {

      if (count > 0) {
        for (tab = count/TAB; tab > 0; --tab)
          printf("\t");
        for (blank = count%TAB; blank > 0; --blank)
          printf(" ");
      }
      count = 0;

      printf("%c", input);
    }
  }

  return 0;
}
