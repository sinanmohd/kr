#include <stdio.h>

#define MAXLINE 1000

void reverse(char str[]);
void str_cp(char to[], char from[]);

int
main(void)
{
	int input, i;
	char str[MAXLINE];

	i = 0;

	while ((input = getchar()) != EOF) {
		str[i] = input;
		i++;

		if (input == '\n') {
                        str[i] = '\0';
			i = 0;
			reverse(str);
                        printf("%s\n", str);
		}
	}

	return 0;
}

void 
str_cp(char to[], char from[])
{
	int i, last_non_blank;

        last_non_blank = -1;

	for (i = 0; from[i] != '\0'; ++i) {
          if (from[i] != ' '&& from[i] != '\n' && from[i] != '\t')
            last_non_blank = i;

          to[i] = from[i];
        }
		
        if (last_non_blank >= 0)
          to[last_non_blank+1] = '\0';
        else
          to[i] = '\0';
}

void
reverse(char str[])
{
	int i, len;
	char storage[MAXLINE];

        i = len = 0;

	str_cp (storage, str);
	
	for ( len = 0; storage[len] != '\0'; len++)
		;

	for ( i = 0; i < len; ++i)
		str[i] = storage[len-1-i];

	str[i] = '\0';
}

	
