#include <stdio.h>
#include <ctype.h>

#define MAXLEN 64

int sneed_getline(char str[], int max);
double atof(const char str[]);

int 
main(void)
{
  char str[MAXLEN];

  while (sneed_getline(str, MAXLEN) > 0)
    printf("%f\n\n", atof(str));

  return 0;
}

int
sneed_getline(char str[], int max)
{
  char input;
  char *str_og = str;

  while (--max && (*str = input = getchar()) != EOF && input != '\n')
    str++;

  if (!max && input != '\n')
    *str = '\n', str++;

  str = '\0';

  return str - str_og;
}

double 
atof(const char str[])
{
  int i, sign, e_sign, j;
  double val, power, e_power;

  for (i = 0; isspace(str[i]); i++)
    ;
  sign = (str[i] == '-') ? -1 : 1;

  if (str[i] == '-' || str[i] == '+')
    i++;

  for(val = 0.0; isdigit(str[i]); i++)
    val = val * 10.0 + (str[i] - '0');

  if (str[i] == '.')
    i++;

  for(power = 1.0; isdigit(str[i]); i++) {
    val = 10 * val + (str[i] - '0');
    power *= 10.0;
  }

  if (str[i] == 'e' || str[i] == 'E')
    i++;

  e_sign = (str[i] == '-') ? -1 : 1;

  if (str[i] == '-' || str[i] == '+')
    i++;

  for (e_power = 0.0; isdigit(str[i]); i++)
    e_power = e_power * 10 + (str[i] - '0');

  if (e_sign < 0)
    for (j = 0; j < e_power; j++)
      power *= 10.0;
  else
    for (j = 0; j < e_power; j++)
      power /= 10.0;

  return (sign * val / power);
}
