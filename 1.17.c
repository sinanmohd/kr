#include <stdio.h>

#define MAXLINE 1000

int gline(char str[], int len);

int 
main(void)
{
  int len;
  char str[MAXLINE];

  while ((len = gline(str, MAXLINE)) > 0)
    if (len > 80)
      printf("\nlength: %d\n%s\n", len, str);

  return 0;
}

int 
gline(char str[], int len)
{
  int i;
  char input;
  for (i = 0; i < len-1 && (input = getchar()) != EOF && input != '\n'; i++)
    str[i] = input;

  if (input == '\n' && i < len-1) {
    str[i] = '\n';
    ++i;
  }
  
  str[i] = '\0';

    return i;
}
