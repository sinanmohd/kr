#include <stdio.h>

#define INT_NBITS 32
#define ONE 1

unsigned getbits(unsigned x, int from, int to);
void pbits(int x);
unsigned rightrot(unsigned x, int n);

int 
main(void)
{
  int x = 2394234;

  pbits(x);

  pbits(rightrot(x, 8));
  
  return 0;
}

unsigned 
getbits(unsigned x, int from, int to)
{
  return (x >> (from+1-to)) & ~(~(unsigned)0 << to);
}

void 
pbits(int x)
{
  for (int i = INT_NBITS-1; i >= 0; i--) {
    if (i%4 == 3)
      printf(" ");

    printf("%u", getbits(x, i, 1));
  }

  printf("\n");
}

unsigned 
rightrot(unsigned x, int n)
{
  int model = (x >> n) << n;
  for (int i = 1; i <= n; i++) {
    int shift = n+1-i*2;

    if (shift >= 0)
      model |= ((x & (ONE << (n-i))) >> shift);
    else
      model |= ((x & (ONE << (n-i))) << -shift);
  }

  return model;
}
