#include <stdio.h>
 
#define MAXLEN 1000

char* expand(char coom[], char str[]);

int 
main(void)
{
  char coom[] = "sneed a-z a-b-c a-z0-9 -a-z sneed";
  char str[MAXLEN];

  printf("%s\n%s\n", coom, expand(coom, str));

  return 0;
}

char*
expand(char from[], char to[])
{
  int i, j, k;

  for (i = 0, j = 0; from[i] != '\0'; i++) {
    to[j++] = from[i];

    for (k = from[i]+1; ((from[i] >= '0' && from[i] <= '9') || (from[i] >= 'a' && from[i] <= 'z') || (from[i] >= 'A' && from[i] <= 'Z')) && from[i+1] == '-' && k < from[i+2]; k++, j++)
      to[j] = k;

    if (from[i+1] == '-' && ((from[i] >= '0' && from[i] <= '9') || (from[i] >= 'a' && from[i] <= 'z') || (from[i] >= 'A' && from[i] <= 'Z')))
      i++;
  }

  to[j] = '\0';

  return to;
}
