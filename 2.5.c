#include <stdio.h>

/* function to delete  each character in str1 that matches with str2 */
int any(const char s1[], const char s2[]);

int 
main(void)
{
  char str[] = "ok chud you win this time";
  char del[] = "ic";

  printf("%d\n", any(str, del));

  return 0;
}

int 
any(const char s1[], const char s2[])
{
  int i, j;

  for (i = 0; s1[i] != '\0'; i++) {

    for (j = 0; s2[j] != '\0'; j++)
      if (s2[j] == s1[i])
        return i+1;

  }

  return -1;
}

