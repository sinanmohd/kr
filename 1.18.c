#include <stdio.h>

#define MAXLINE 1000

int 
main(void)
{
  int input, i, non_blank;
  char str[MAXLINE];

  i = 0;
  non_blank = -1;

  while ((input = getchar()) != EOF) {
    while (input != '\n' && input != EOF) {
      str[i] = input;

      if (input != ' ' && input != '\t')
        non_blank = i;

      ++i;
      input = getchar();
    }

    if (non_blank == -1)
      continue;

    str[non_blank+1] = '\0';
    printf("%s\n", str);
    
    i = 0;
  }

  return 0;
}
