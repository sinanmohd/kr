#include <stdio.h>

#define MAXLEN 1000

int get_line(char line[], const int max);
int strindex(char source[], char pattern[]);

int 
main(void)
{
  int found, rightmost;
  char line[MAXLEN];

  char pattern[] = "ould";
  found  = 0;

  while (get_line(line, MAXLEN) > 0)
    if ((rightmost = strindex(line, pattern)) >= 0) {
      printf("%s >> match at char %d\n", line, rightmost+1);
      found++;
    }

  printf("\nSneedGrep found %d match%s\n", found, (found > 0) ? "es" : "");

  return 0;
}

int 
get_line(char line[], const int max)
{
  int i;
  char input;

  for (i = 0; (input = getchar()) != EOF && input != '\n' && i < max-2; i++)
    line[i] = input;

  if (input == '\n' && i < max-2)
    line[i++] = '\n';

  line[i] = '\0';

  return i;
}

int 
strindex(char source[], char pattern[])
{
  int i, j, k, rightmost;

  rightmost = -1;

  for (i = 0; source[i] != '\0'; i++) {
    for (j = i, k = 0; pattern[k] != '\0' && source[j] == pattern[k]; j++, k++)
      ;

    if (pattern[k] == '\0')
      rightmost = i;
  }

  return rightmost;
}
