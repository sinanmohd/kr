#include <stdio.h>
#include <string.h>

#define MAXLEN 15

void reverse(char str[], size_t left, size_t right);
char* itoa(int n, char str[]);
void swap(char array[], int i, int j);

int 
main(void)
{
  int n = 42069;
  char str[MAXLEN];

  printf("%d\n", n);
  printf("%s\n", itoa(n, str));

  return 0;
}

void
reverse(char str[], size_t left, size_t right)
{
  if (left >= right)
    return;

  swap(str, left, right);
  reverse(str, left+1, right-1); 
}

char*
itoa(int n, char str[])
{
  int sign, i;

  /* to include the most -ve int */
  n = ((sign = n) < 0) ? -(n+1) : n-1;

  i = 0;
  do {
    str[i++] = n%10 + '0';
  } while ((n /= 10) > 0);

  if (sign <0)
    str[i++] = '-';

  /* part of "to include the most -ve int" */
  str[0] += 1;
  str[i] = '\0';

  reverse(str, 0, strlen(str)-1);

  return str;
}

void
swap(char array[], int i, int j)
{
  int temp;

  temp = array[i];

  array[i] = array[j];
  array[j] = temp;
}
