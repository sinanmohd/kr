#include <stdio.h>

#define MAXLINE 1000

int gline(char str[], int len);
void cp_str(char to[], char from[]);

int 
main(void)
{
  int len, max;
  char str[MAXLINE];
  char mstr[MAXLINE];

  max = 0;

  while ((len = gline(str, MAXLINE)) > 0)
    if (len > max) {
      cp_str(mstr, str);
      max = len;
    }

  if (max > 0)
    printf("\nlength: %d\n%s", max, mstr);

  return 0;
}

int 
gline(char str[], int len)
{
  int i;
  char input;

  i = 0;

  while (i < len-1) {
    input = getchar();

    if (input == EOF)
      break;
    else if (input == '\n')
      break;

    str[i] = input;
    i++;
  }
    

  if (input == '\n' && i < len-1) {
    str[i] = '\n';
    ++i;
  }
  
  str[i] = '\0';

    return i;
}

void
cp_str(char to[], char from[])
{
  for(int i =0; (to[i] = from[i]) != '\0'; i++)
    ;
}
