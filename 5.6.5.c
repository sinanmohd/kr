#include <stdio.h>
#include <ctype.h>

#define MAXLEN 1000

int atoi(char str[]);
int sneed_getline(char str[], int max);

int 
main(void)
{
  char str[MAXLEN];
  while (sneed_getline(str, MAXLEN)) {
    printf("string ver : %s", str);
    printf("int    ver : %d\n", atoi(str));
  }
  return 0;
}

int 
atoi(char str[])
{
  int val;

  for (val = 0; isdigit(*str) && *str; str++)
    val = val * 10 + *str -'0';

  return val;
}

int
sneed_getline(char str[], int max)
{
  char input;
  char *str_og = str;

  while (--max && (*str = input = getchar()) != EOF && input != '\n')
    str++;

  if (!max && input != '\n')
    *str = '\n', str++;

  str = '\0';

  return str - str_og -1;
}

