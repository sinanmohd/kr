#include <stdio.h>

int binsearch(int x, int v[], int n);
int binsearch2(int x, int v[], int n);

int 
main(void)
{
  int v[10]= {1, 2, 3, 4, 5, 6, 7, 8, 9, 11};
  printf("%d\n", binsearch(v[5], v, 10));
}

int 
binsearch(int x, int v[], int n) 
{
  int low, high, mid;

  low = 0;
  high = n-1;

  while (low <= high) {
    mid = (low+high)/2;

    if (x < v[mid])
      high = mid - 1;
    else if (x > v[mid])
      low  = mid + 1;
    else 
      return mid;
  }

  return -1;
}

int 
binsearch2(int x, int v[], int n) 
{
  int low, high, mid;

  low = 0;
  high = n-1;

  while (low < high) {
    mid = (low+high)/2;

    if (x <= v[mid])
      high = mid;
    else 
      low = mid+1;

  }

  /* here low == high */
  
  return (x == v[low]) ? low : -1; 
}
