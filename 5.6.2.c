#include <stdio.h>

#define MAXLEN 15

void reverse(char str[]);
char* itoa(int n, char str[]);

int 
main(void)
{
  int n = -147483647;
  char str[MAXLEN];

  printf("%d\n", n);
  printf("%s\n", itoa(n, str));

  return 0;
}

void
reverse(char str[])
{
  char temp, *str_og;

  str_og = str;

  while (*str++)
    ;
  str -= 2;

  while (str_og < str) {
    temp = *str_og;
    *str_og++ = *str;
    *str-- = temp;
  }
}

char*
itoa(int n, char str[])
{
  int sign;
  char *str_og = str;

  /* to include the most -ve int */
  n = ((sign = n) < 0) ? -(n+1) : n-1;

  do {
    *str++ = n%10 + '0';
  } while ((n /= 10) > 0);

  if (sign <0)
    *str++ = '-';

  /* part of "to include the most -ve int" */
  *str_og += 1;
  *str = '\0';

  reverse(str_og);

  return str_og;
}
