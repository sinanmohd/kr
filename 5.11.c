#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define DEFTAB 8
#define TRUE 1
#define FALSE 0
#define MAXSTOPS 100

int stops[MAXSTOPS];

void detab(int *stops, int limit);
void entab(int *stops, int limit);

int main(int argc, char **argv)
{
        int entab_bool = FALSE;
        int detab_bool = FALSE;
        int stop_position = 0;

        while (--argc > 0 && **++argv == '-') {
                while (*++*argv) {
                        switch (**argv) {
                                case 'e' :
                                        entab_bool = TRUE;
                                        break;
                                case 'd' :
                                        detab_bool = TRUE;
                                        break;
                                case 's' :
                                        if (*++*argv != '=')
                                                break;
                                        
                                        while (*++*argv && stop_position < MAXSTOPS) {
                                                stops[stop_position++] = atoi(*argv);
                                                printf("loooop -- %d\n", stop_position);

                                                while (isdigit(**argv))
                                                        ++*argv;

                                                if (**argv == '\0')
                                                        --*argv;
                                        }
                                        break;
                        }
                }
        }

        stops[stop_position] = DEFTAB;

        if (entab_bool ^ detab_bool) {
                if (entab_bool)
                        entab(stops, stop_position);
                else if (detab_bool)
                        detab(stops, stop_position);
        } else {
                printf("usage: tab -[ed] -s=s1,s2,s3,...,s3\n");
        }

        return 0;
}

void detab(int *stops, int limit)
{
        char input;
        int space, count = 0, stop_position = 0;

        while((input = getchar()) != EOF) {
                switch (input) {
                        case '\t' :
                                for (space = count%stops[stop_position]; space < stops[stop_position]; space++)
                                        putchar(' ');
                                count = 0;

                                if (stop_position < limit)
                                        stop_position++;

                                break;
                        case '\n' :  
                                count = 0;
                                break;
                        default :
                                putchar(input);
                                count++;
                                break;
                                
                }
        }
}

void entab(int *stops, int limit)
{
        char input;
        int spaces = 0, stop_position = 0, count = 0;

        while((input = getchar()) != EOF) {
                switch (input) {
                        case ' ' :
                                spaces++;
                                break;
                        case '\n' :
                                count = 0;
                                putchar('\n');
                                break;
                        default :
                                // add spaces misssed by the first tab
                                if (spaces >= stops[stop_position])
                                        spaces += count%stops[stop_position];

                                while (spaces/stops[stop_position]) {
                                        spaces = spaces - stops[stop_position]; 

                                        putchar('\t');

                                        if (stop_position < limit)
                                                stop_position++;
                                }

                                while (spaces--)
                                        putchar(' ');

                                spaces = 0;
                                count++;
                                putchar(input);
                                break;
                }
        }
}
