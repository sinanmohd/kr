#include <stdio.h>

#define OUT 0
#define FALSE -1
#define TRUE 1
#define STAR 1
#define SLASH 2

void count(char input, int* parentheses, int* brackets, int* braces);

/* this is a c programm to detect syntax errors in c programms */

int 
main(void)
{
  char input;
  int temp, state, line, parentheses, brackets, braces, single_quotes, double_quotes, double_quotes_line;

  state = OUT;
  parentheses = brackets = braces = single_quotes = double_quotes = double_quotes_line = 0;
  line = 1;

  while ((input = getchar()) != EOF) {

    if (input == '\n') {
      line++;
      continue;
    }

    /* filiter out comments */

    else if (input == '/') {
       temp = input;

      if ((input = getchar()) == '*') {
        state = STAR;
        temp = FALSE;
      } 

      else if (input == '/') {
        state = SLASH;
        temp = FALSE;
      }

    }

    else if (input == '*' && state == STAR) {

      if ((input = getchar()) == '/') {
        state = OUT;
        continue;
      }  

    }

    else if (input == '\n' && state == SLASH) {
      state = OUT;
      continue;
    }

    if (temp != FALSE)
      count(temp, &parentheses, &brackets, &braces);

    if (state == OUT) {

      if (input == '"' && double_quotes == TRUE) {
        double_quotes = FALSE;
        continue;
      } 

      /* process escape sequences  */

      else if (input == '\\') {
        input = getchar();

        if (input == '\n') {
          line++;
          continue;
        }

        if (input != 'a' && input != 'b' && input != 'f' && input != 'n' && input != 'r' &&
            input != 't' && input != 'v' && input != '\\' && input != '\'' && input != '"' && input != '?' && input != '0')
          printf("unproper escape sequence detected at line %d\n", line);
        
        else if (input == 'o') {
          if((input = getchar()) != 'o' || (input = getchar()) != 'o')
            printf("unproper escape sequence detected at line %d\n", line);
        }

        else if (input == 'x') {
          if((input = getchar()) != 'h' || (input = getchar()) != 'h')
            printf("unproper escape sequence detected at line %d\n", line);
        }

        else 
          continue;
      }

      /* process double quotes */

      else if (input == '"') {
        double_quotes = TRUE;
        double_quotes_line = line;
      }

      /* process single quotes */ 

      else if (input == '\'' && double_quotes != TRUE) {
        ;
        if ((input = getchar()) == '\\') {
          input = getchar();
        }

        if ((input = getchar()) != '\'')
          printf("unproper single quotes detected at line %d\n", line);

        continue;
      }

      /* process all others */

      count(input, &parentheses, &brackets, &braces);
    }
  }
  
  if (parentheses%=2)
    printf("unproper parentheses detected\n");
  if (brackets%=2)
    printf("unproper brackets detected\n");
  if (braces%=2)
    printf("unproper braces detected\n");
  if (double_quotes == TRUE)
    printf("unproper double quotes detected at line %d\n", double_quotes_line);

  return 0;
}

void 
count(char input, int* parentheses, int* brackets, int* braces)
{
  switch (input) {
        case '(' :
          (*parentheses)++;
          break;
        case ')' :
          (*parentheses)--;
          break;
        case '[' :
          (*brackets)++;
          break;
        case ']' :
          (*brackets)--;
          break;
        case '{' :
          (*braces)++;
          break;
        case '}' :
          (*braces)--;
          break;
  }
}
