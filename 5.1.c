#include <stdio.h>
#include <ctype.h>

#define MAXBUFF 100

int getint(int *pn);
int getch(void);
void ungetch(int input);

int main(void)
{
        int input;

        getint(&input);
        printf("input: %d\n", input);

        return 0;
}

int getint(int *pn)
{
        char input, sign;

        while (isspace(input = getch()))
                ;

        if (!isdigit(input) && input != EOF && input != '+' && input != '-') {
                ungetch(input);
                return 0;
        }

        sign = (input == '-') ? -1 : 1;

        if (input == '-' || input == '+') {
                input = getch();

                if (!isdigit(input)) {
                        ungetch((sign == -1) ? '-' : '+');
                        return 0;
                }
        }

        for (*pn = 0; isdigit(input); input = getch())
                *pn = (*pn * 10) + (input - '0');

        *pn *= sign;

        if (input != EOF)
                ungetch(input);

        return input;
}

char buff[MAXBUFF];
int bpos = 0;

int getch(void)
{
        return (bpos) ? buff[--bpos] : getchar();
}

void ungetch(int input)
{
        if (bpos < MAXBUFF)
                buff[bpos++] = input;
        else
                printf("err: buff full\n");
}
