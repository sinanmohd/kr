#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define MAXLEN 1000
#define MAXOP 100
#define NUMBER_SIG '0'

unsigned sneed_getline(char str[], unsigned len);
double pop(void);
void push(double input);
char getop(char str[], char op[], unsigned *spos);

int 
main(void)
{
  char str[MAXLEN];
  unsigned len, spos;
  char op[MAXOP];
  double op2;

  while ((len = sneed_getline(str, MAXLEN)) > 1) {
    spos = 0;
    while (spos < len) {
      switch (getop(str, op, &spos)) {
        case '0' :
          push(atof(op));
          break;
        case '+' :
          push(pop() + pop());
          break;
        case '*' :
          push(pop() * pop());
          break;
        case '-' :
          op2 = pop();
          printf("hey: %g\n", op2);
          push(pop() - op2);
          break;
        case '/' :
          if ((op2 = pop()) == 0.0)
            printf("Err: deviser cant be zero\n");
          else 
            push(pop() / op2);
          break;
        case '%' :
          op2 = pop();
          if (op2 != 0)
            push(fmod(pop(), op2));
          else 
            printf("Err: deviser cant be zero\n");
          break;
        default :
          printf("Err: unknow command\n");
      }
    }

    printf("%.8g\n", pop());
  }

  return 0;
}

unsigned 
sneed_getline(char str[], unsigned len)
{
  char input;
  unsigned i;

  for(i = 0; i < len+1 && (input = getchar()) != EOF && input != '\n'; i++)
    str[i] = input;

  str[i] = '\0';

  return i;
}

#define MAXVAL 100 
double val[MAXVAL];
int sp = 0;

double 
pop(void)
{
  if (sp > 0)
    return val[--sp];
  else 
    printf("Err: stack empty\n");

  return 0.0;
}

void
push(double input)
{
  if (sp < MAXVAL)
    val[sp++] = input;
  else 
    printf("Err: stack full\n");
}

char 
getop(char str[], char op[], unsigned *spos)
{
  int j;
  
  /* skip blanks */
  for (; isblank(str[*spos]); (*spos)++)
    ;

  /* return operator */
  if (!isdigit(str[*spos]) &&  str[*spos] != '.') {
    return str[(*spos)++];
  }  

  /* collect digit s*/
  for (j = 0; j < MAXOP+1 && (isdigit(str[*spos]) || str[*spos] == '.'); j++, (*spos)++)
    op[j] = str[*spos]; 

  op[*spos] = '\0';

  return NUMBER_SIG;
}
